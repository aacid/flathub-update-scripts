#!/usr/bin/env python3

import sys
import subprocess
import json
import os
import requests

page=1
while True:
    r = requests.get("https://api.github.com/orgs/flathub/repos?per_page=100&page=" + str(page) ,
                                headers={"Accept" : "application/vnd.github+json", "Authorization" : "Bearer YOUR_TOKEN"})

    repos = json.loads(r.content)
    if not repos:
        break
    for repo in repos:
        if repo["name"].startswith("org.kde."):
            print(repo["name"])

    page=page+1
